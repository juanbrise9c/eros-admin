var s = {}
var api = 'https://erostechnology.xyz/api/?'

var getFilter = '_GET&filter='
var patchFilter = '_PATCH&filter='
var postFilter = '_POST&filter='
var fileFilter = '_FILE'
var path = 'PATH='
var u = '________u='

var gnStartX = 0
var gnStartY = 0
var gnEndX = 0
var gnEndY = 0
var xAside = 0
var xAsideAux = 0

jQuery(document).ready(function($) {
    FastClick.attach(document.body)
    riot.mount('index')
    riot.mount('aside')
    setTimeout(function() {
        G.setNavbar('navbar-index')
        app.panel.open('aside.panel')
    }, 500)

    $('body').on('touchend',function(e,data) {
        endToushAside(e)
    })

    $('body').on('touchstart', function(event){
        gnStartX = event.touches[0].pageX
        gnStartY = event.touches[0].pageY
        event.preventDefault()
    })

    $('body').on('touchmove', function(event){
        if (!$('aside').hasClass('panel-in'))
            return

        gnEndX = event.touches[0].pageX
        gnEndY = event.touches[0].pageY  
        var distance = Math.ceil(nthroot(Math.pow((gnEndX - gnStartX),2) + Math.pow((gnEndY - gnStartY),2), 2))

        if(Math.abs(gnEndX - gnStartX) > Math.abs(gnEndY - gnStartY)) {
            if(gnEndX > gnStartX) {
                xAside += distance
                // alert("Swipe Right - Distance " + distance + 'px')
            } else {
                xAside -= distance
                // alert("Swipe Left - Distance " + distance + 'px')     
            }
        } else {
            // if(gnEndY > gnStartY) {
            //     alert("Swipe Bottom - Distance " + distance + 'px')  
            // } else {
            //     alert("Swipe Top - Distance " + distance + 'px')      
            // }        
        }  
        gnStartX = event.touches[0].pageX
        gnStartY = event.touches[0].pageY

        if (xAside > 0)
            xAside = 0

        $('aside').css('left', xAside+'px')

        event.preventDefault()      
    })
})

function nthroot(x, n) {
  try {
    var negate = n % 2 == 1 && x < 0;
    if(negate)
      x = -x;
    var possible = Math.pow(x, 1 / n);
    n = Math.pow(possible, n);
    if(Math.abs(x - n) < 1 && (x > 0 == n > 0))
      return negate ? -possible : possible;
  } catch(e){}
}

function endToushAside() {
    if (!$('aside').hasClass('panel-in'))
        return

    if (xAside <= -30) {
        app.panel.close()
        xAside = 0
        $('aside').wait(500).css('left', xAside+'px')
    }else{
        xAside = 0
        $('aside').css('left', xAside+'px')
    }
}

function hash(_hash) {
	window.location.hash = '#/'+_hash
}

document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {
    window.open = cordova.InAppBrowser.open;
}

$( window ).on( 'hashchange', function( e ) {
    var h = window.location.hash
    popup.close()
    
    switch(h){
    	case '#/':
        case '#/index':
        case '':
            G.goHash('index')
            break;

        case '#/requests':
            G.goHash('requests')
            break;
       
        case '#/appointments':
            G.goHash('appointments')
            break;

        case '#/users':
            G.goHash('users')
            break;

        case '#/user':
            G.openPopUp('user')
        break;

        case '#/partners':
            G.goHash('partners')
        break;

        case '#/settings':
            G.openPopUp('settings')
        break;
    }
});