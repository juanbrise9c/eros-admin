var app,
	view,
	$$,
	$ptrordersContent,
	popup,
	popupL2,
	loginScreen,
	token

function SYNC() {
	if(localStorage.getItem('erosAdmin') != null){
		token = JSON.parse(localStorage.getItem('erosAdmin'))
	}else{
		loginScreen.open()
	}
	setTimeout(function() {
		$('.panel-backdrop').attr('class', 'panel-backdrop');
	}, 1500)
}

jQuery(document).ready(function($) {
	try{
		app = new Framework7({
			root: '#app',
			theme : 'md',
			name: 'OP Ecologia',
			dialog: {
				buttonOk: 'Aceptar',
				buttonCancel: 'Cancelar'
			},
			data: function() {},
			methods: {},
			routes: routes,
			toolbar: {
				hideOnPageScroll: true,
			}
		})
		
		$$ = Dom7
		view = app.views.create('.view-main')
		app.searchbar.create({
							backdrop: true,
							backdropEl: '#searchbar-backdrop-index',
							expandable:true,
							el: '#searchbar-index',
						})

		popup = app.popup.create({
			content: '<div class="popup" id="popup"></div>',
			on: {
				opened: function () {
					console.log('Popup opened')
				}
			}
		})

		popupL2 = app.popup.create({
			content: '<div class="popup" id="popupL2"></div>',
			on: {
				opened: function () {
					console.log('Popup opened')
				}
			}
		})

		loginScreen = app.loginScreen.create({
			content: '<div class="login-screen"><login></login></div>',
			on: {
				open: function () {
					riot.mount('login')
				}
			}
		})
	
		SYNC()
	}catch(e){
		console.log(e)
	}
})

