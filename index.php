<?php 
	$version = uniqid();
?>
<!DOCTYPE html>
<html lang="es" class="md device-pixel-ratio-3 device-ios theme-light with-panel">
<head>
	<script src="cordova.js"></script>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, viewport-fit=cover">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	<META HTTP-EQUIV="EXPIRES" CONTENT="Mon, 22 Jul 2002 11:12:01 GMT">
	<title>EROS</title>
	<link rel="stylesheet" href="core.css?v=<?php echo $version; ?>">
	<link rel="stylesheet" href="assets/css/iconfont.css">
	<link rel="stylesheet" href="assets/css/eros.css">
	<script src="assets/js/f7.js"></script>
	<script src="assets/js/jquery-3.2.1.min.js"></script>
	<script src="assets/js/wait.js"></script>
	<script src="assets/js/fastclick.js"></script>
	<script src="assets/js/riot.js"></script>
	
	
	<script src="js/G.js?v=<?php echo $version; ?>"></script>
	<script src="assets/js/numeral.min.js"></script>
	<script src="assets/js/moment.js"></script>

	<!-- ONESIGNAL WEB 
	<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js"></script>
	<script>
		
		
	  var OneSignal = window.OneSignal || [];
	  OneSignal.push(function() {
	    OneSignal.init({
			appId: "4c0639ad-8d42-40e1-b8e6-5c84b858903b",
			safari_web_id: "web.onesignal.auto.37bbdda8-1be5-416a-8d2a-3d51b0669a43",
			notifyButton: {
			enable: true,
			},
	      subdomainName: "deliveryorder.os.tc", /* The label for your site that you added in Site Setup mylabel.os.tc */
	      promptOptions: {
	        /* These prompt options values configure both the HTTP prompt and the HTTP popup. */
	        /* actionMessage limited to 90 characters */
	        actionMessage: "Nos gustaría mostrarle notificaciones de las últimas noticias y actualizaciones.",
	        /* acceptButtonText limited to 15 characters */
	        acceptButtonText: "Permitir",
	        /* cancelButtonText limited to 15 characters */
	        cancelButtonText: "Después"
	      }
	    });
	    OneSignal.showSlidedownPrompt();
	  });
	</script>-->
	<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
<script>
  window.OneSignal = window.OneSignal || [];
  OneSignal.push(function() {
    OneSignal.init({
      appId: "4c0639ad-8d42-40e1-b8e6-5c84b858903b",
      safari_web_id: "web.onesignal.auto.37bbdda8-1be5-416a-8d2a-3d51b0669a43"
    });
  });
</script>


    <script src="config.js?v=<?php echo $version; ?>"></script>
	<script type="riot/tag" src="pages/body/login.html?v=<?php echo $version; ?>"></script>

	<!-- INDEX -->
	<script type="riot/tag" src="pages/body/aside.html?v=<?php echo $version; ?>"></script>
    <script type="riot/tag" src="pages/body/index.html?v=<?php echo $version; ?>"></script>
	<script type="riot/tag" src="pages/navbars/index.html?v=<?php echo $version; ?>"></script>
	
	<!-- REQUESTS -->
	<script type="riot/tag" src="pages/body/requests.html?v=<?php echo $version; ?>"></script>
	<script type="riot/tag" src="pages/navbars/requests.html?v=<?php echo $version; ?>"></script>
	
	<!-- APPOINTMENTS -->
	<script type="riot/tag" src="pages/body/appointments.html?v=<?php echo $version; ?>"></script>
	<script type="riot/tag" src="pages/navbars/appointments.html?v=<?php echo $version; ?>"></script>
	
	<!-- USERS -->
	<script type="riot/tag" src="pages/body/users.html?v=<?php echo $version; ?>"></script>
	<script type="riot/tag" src="pages/navbars/users.html?v=<?php echo $version; ?>"></script>
	<script type="riot/tag" src="pages/popups/user.html?v=<?php echo $version; ?>"></script>
	
	<!-- PARTNERS -->
	<script type="riot/tag" src="pages/body/partners.html?v=<?php echo $version; ?>"></script>
	<script type="riot/tag" src="pages/navbars/partners.html?v=<?php echo $version; ?>"></script>

	<!-- SETTINGS -->
	<script type="riot/tag" src="pages/popups/settings.html?v=<?php echo $version; ?>"></script>

	<head>
</head>
<body>
	<div id="app">

		<aside></aside>
		
		<div class="view view-init view-main">
	
			<!-- page -->
			<div class="page">
				<div id="navbars"></div>
				<div class="searchbar-backdrop" onclick="G.hideSearchbar('products')"></div>
				<div id="pages" class="page-content">
					<index></index>
				</div>

			</div>

		</div>
	</div>

	<script src="assets/js/functions.js?v=<?php echo $version; ?>"></script>
	<script src="js/routes.js?v=<?php echo $version; ?>"></script>
	<script src="js/app.js?v=<?php echo $version; ?>"></script>

</body>
</html>