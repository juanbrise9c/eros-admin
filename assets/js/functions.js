function uniqid(a = "",b = false){
    var c = Date.now()/1000;
    var d = c.toString(16).split(".").join("");
    while(d.length < 14){
        d += "0";
    }
    var e = "";
    if(b){
        e = ".";
        var f = Math.round(Math.random()*100000000);
        e += f;
    }
    return a + d + e;
}


var imageResized, imageDataUrl;

var dataURLToBlob = function(dataURL) {
     var BASE64_MARKER = ';base64,';
     if (dataURL.indexOf(BASE64_MARKER) == -1) {
          var parts = dataURL.split(',');
          var contentType = parts[0].split(':')[1];
          var raw = parts[1];

          return new Blob([raw], {type: contentType});
     }

     var parts = dataURL.split(BASE64_MARKER);
     var contentType = parts[0].split(':')[1];
     var raw = window.atob(parts[1]);
     var rawLength = raw.length;

     var uInt8Array = new Uint8Array(rawLength);

     for (var i = 0; i < rawLength; ++i) {
          uInt8Array[i] = raw.charCodeAt(i);
     }

     return new Blob([uInt8Array], {type: "image/jpg"});
     //return new Blob([uInt8Array], {type: contentType});
}

function arraymove(arr, fromIndex, toIndex) {
    var element = arr[fromIndex];
    arr.splice(fromIndex, 1);
    arr.splice(toIndex, 0, element);
}

function array_move(arr, old_index, new_index) {
    if (new_index >= arr.length) {
        var k = new_index - arr.length + 1;
        while (k--) {
            arr.push(undefined);
        }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr; // for testing
}


function idGenerator(x){
    if (x === null) {
        return uniqid()
    }else if (x === undefined || x.length == 0){
        return uniqid()
    }else{
        if (!("id" in x))
            return uniqid()

        if (x.id == "")
            return uniqid()

        return x.id
    }
}
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function hash(url, params) {
    window.location.hash = '/' + url;
}

Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0, 10);
});


function AllowOnlyNumbers(e) {

e = (e) ? e : window.event;
var clipboardData = e.clipboardData ? e.clipboardData : window.clipboardData;
var key = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
var str = (e.type && e.type == "paste") ? clipboardData.getData('Text') : String.fromCharCode(key);

return (/^\d+$/.test(str));
}

function onlyNumber(evt) {
    var ch = String.fromCharCode(evt.which);
    if (!/[0-9]/.test(ch)) {
        evt.preventDefault();
    }
}

var toastInterval = null;

function reToast() {
    $('#toast')
        .css('transform', 'translateY(100px)')
    clearInterval(toastInterval);
}

function participar() {
    $('#login').css('display', 'flex').wait(20).css('opacity', '1');
}

function reverseForIn(obj, f) {
  var arr = [];
  for (var key in obj) {
    // add hasOwnPropertyCheck if needed
    arr.push(key);
  }
  for (var i=arr.length-1; i>=0; i--) {
    f.call(obj, arr[i]);
  }
}


function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}
function toast(text, duracion) {
    if (duracion == undefined) {
        nToast = 5000;
    } else {
        nToast = duracion;
    }
    reToast();
    $('#toast')
        .show()
        .wait(300)
        .html(text)
        .css('transform', 'translateY(0)')

    toastInterval = setTimeout(function() {
        $('#toast')
            .css('transform', 'translateY(100px)')
            .wait(300)
            .hide();
    }, nToast);
}

function closeToast() {
    $('#toast')
        .css('transform', 'translateY(100px)')
        .wait(300)
        .hide();
}




function load(bol) {
    if (bol) {
        $('#miniLoading')
            .css('display', 'flex')
            .wait(20)
            .css('opacity', '1')
            .find('img')
            .wait(100)
            .css('transform', 'scale(1)')
            .css('opacity', '1')
    }else{
        $('#miniLoading img')
            .css('transform', 'scale(0.8)')
            .css('opacity', '0')
            .parent()
            .wait(100)
            .css('opacity', '0')
            .wait(310)
            .hide()
    }
}




var toastInterval = null;
function reToast() {
    $('#toast')
        .css('transform', 'translateY(100px)')
    clearInterval(toastInterval);
}
function toast(text, duracion){
    if (duracion == undefined) {
        nToast = 5000;
    }else{
        nToast = duracion;
    }
    reToast();
    $('#toast')
        .show()
        .wait(300)
        .html(text)
        .css('transform', 'translateY(0)')
        
    toastInterval = setTimeout(function() {
        $('#toast')
            .css('transform', 'translateY(100px)')
            .wait(300)
            .hide();
    }, nToast);
}
function closeToast(){
    $('#toast')
        .css('transform', 'translateY(100px)')
        .wait(300)
        .hide();
}


function setMyPosition() {
    map.setView(my_position._latlng, 17);
}

function getFormData($form){
    var unindexed_array = $(`#${$form}`).serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}

function resfreshToken(newToken) {
    token = newToken
    token.id = btoa(newToken.id);
    localStorage.setItem('afiliateTokenDelivery', JSON.stringify(token));
    token.productCategories = JSON.parse(token.productCategories)
    token.questionnaires = JSON.parse(token.questionnaires)
    token.id = atob(newToken.id);
    
}

var inProgress = false;
function sendForm(url, form_id, _done, _fail) {
    var form = document.querySelector('#' + form_id);
    var request = new XMLHttpRequest();
    var formData = new FormData(form);
    console.log("formData");

    request.upload.addEventListener('load', function(e) {
        console.log("COMPLETADP LOADED");
        console.log(e);
    }, false);

    request.onreadystatechange = function(aEvt) {
        console.log('CHANGE');
        console.log(request.readyState);
        if (request.readyState == 4) {
            inProgress = false;
            if (request.status == 200) {
                console.log(request.responseText)
                var r = JSON.parse(request.responseText);
                console.log(r);
                _done(r);
            } else {
                _fail();
                console.log("Error loading page\n");
            }
        }
    }

    request.onprogress = function(e) {
        if (e.lengthComputable) {
            var percent = (e.loaded / e.total) * 100;
            console.log("%->" + percent);
        }
    };

    if (!inProgress) {
        inProgress = true;
        request.open('post', url, true);
        request.send(formData);
    }
}


Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};



